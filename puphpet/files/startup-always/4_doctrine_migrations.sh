#!/usr/bin/env bash

until curl http://localhost:3306; do
  echo "..."
  sleep 1
done

docker exec ct_php php ./bin/console --env=prod doctrine:migrations:migrate
