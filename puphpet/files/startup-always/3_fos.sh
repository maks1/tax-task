#!/usr/bin/env bash

docker exec ct_php php ./bin/console --env=prod fos:js-routing:dump --format=json --target=assets/js/frontend/routes/fos_js_routes.json
