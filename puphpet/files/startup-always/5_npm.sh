#!/usr/bin/env bash

docker exec ct_node npm config set bin-links false
docker exec -w /app ct_node npm install --no-bin-links --no-warnings --no-audit
docker exec -w /app ct_node npm audit fix
docker exec -w /app ct_node npm rebuild node-sass
docker exec -w /app ct_node npm run dev
