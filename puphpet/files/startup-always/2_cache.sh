#!/usr/bin/env bash

docker exec ct_php php ./bin/console --env=prod cache:clear --no-warmup
docker exec ct_php php ./bin/console --env=prod cache:warmup
