#!/usr/bin/env bash

VAGRANT_CORE_FOLDER=$(cat '/.puphpet-stuff/vagrant-core-folder.txt')
DOCKER_COMPOSE_FILE="${VAGRANT_CORE_FOLDER}/../docker-compose.yml"

docker-compose -f "${DOCKER_COMPOSE_FILE}" stop
docker-compose -f "${DOCKER_COMPOSE_FILE}" rm --force
docker-compose -f "${DOCKER_COMPOSE_FILE}" pull
docker-compose -f "${DOCKER_COMPOSE_FILE}" up -d --remove-orphans
