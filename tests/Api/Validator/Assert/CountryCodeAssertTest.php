<?php

namespace App\Tests\Api\Validator\Assert;

use App\Api\Exception\ApiBadRequestException;
use App\Api\Exception\ApiResourceNotFoundException;
use App\Api\Validator\Assert\CountryCodeAssert;
use App\Entity\Country;
use App\Repository\CountryRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CountryCodeAssertTest extends TestCase
{

    /**
     * @dataProvider getData
     */
    public function testValidate($value, ?string $exceptionClass, ?Country $country)
    {
        /** @var MockObject|CountryRepository $countryRepository */
        $countryRepository = $this->createMock(CountryRepository::class);
        $countryRepository->method('find')->willReturn($country);
        $countryCodeAssert = new CountryCodeAssert($countryRepository);
        if ($exceptionClass) {
            $this->expectException($exceptionClass);
        } else {
            $this->assertTrue(true);
        }
        $countryCodeAssert->validate($value);
    }

    public function getData(): array
    {
        return [
            ['', ApiBadRequestException::class, null],
            [' ', ApiBadRequestException::class, null],
            ['DE', null, new Country('DE', 'EUR')],
            [' DE ', null, new Country('DE', 'EUR')],
            ['UA', ApiResourceNotFoundException::class, null],
            [null, ApiBadRequestException::class, null],
            [5, ApiResourceNotFoundException::class, null]
        ];
    }
}
