<?php

namespace App\Tests\Api\Validator;

use App\Api\Entity\Filter\ApiReportFilter;
use App\Api\Exception\ApiBadRequestException;
use App\Api\Request\ApiReportRequest;
use App\Api\Validator\ApiReportValidator;
use App\Api\Validator\Assert\CountryCodeAssert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ApiReportValidatorTest extends TestCase
{

    public function testValidateEmptyFilter()
    {
        $apiReportRequest = new ApiReportRequest();
        /** @var CountryCodeAssert $countryCodeAssert */
        $countryCodeAssert = $this->createMock(CountryCodeAssert::class);

        $apiReportValidator = new ApiReportValidator($countryCodeAssert);

        $this->expectException(ApiBadRequestException::class);

        $apiReportValidator->validate($apiReportRequest);
    }

    public function testValidateEmptyFilterPeriod()
    {
        /** @var MockObject|ApiReportRequest $apiReportRequest */
        $apiReportRequest = $this->createMock(ApiReportRequest::class);
        $apiReportRequest->method('getFilter')->willReturn(new ApiReportFilter());
        /** @var CountryCodeAssert $countryCodeAssert */
        $countryCodeAssert = $this->createMock(CountryCodeAssert::class);

        $apiReportValidator = new ApiReportValidator($countryCodeAssert);

        $this->expectException(ApiBadRequestException::class);

        $apiReportValidator->validate($apiReportRequest);
    }
}
