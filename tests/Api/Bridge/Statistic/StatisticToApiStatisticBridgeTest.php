<?php

namespace App\Tests\Api\Bridge\Statistic;

use App\Api\Bridge\Statistic\StatisticToApiStatisticBridge;
use App\Entity\Statistic\Statistic;
use PHPUnit\Framework\TestCase;

class StatisticToApiStatisticBridgeTest extends TestCase
{
    /**
     * @dataProvider getData
     */
    public function testBuild(string $period, int $countOfCounties, float $totalAmount, float $avgAmount, float $avgTaxRate)
    {
        $statistic = new Statistic(new \DateTimeImmutable($period));
        $statistic->setCountOfCounties($countOfCounties);
        $statistic->setTotalAmount($totalAmount);
        $statistic->setAvgAmount($avgAmount);
        $statistic->setAvgTaxRate($avgTaxRate);

        $statisticToApiStatisticBridge = new StatisticToApiStatisticBridge();

        $apiStatistic = $statisticToApiStatisticBridge->build($statistic);

        $this->assertEquals($statistic->getCountOfCounties(), $apiStatistic->getCountOfCounties());
        $this->assertEqualsWithDelta($statistic->getTotalAmount(), $apiStatistic->getTotalAmount(), 0.005);
        $this->assertEqualsWithDelta($statistic->getAvgAmount(), $apiStatistic->getAvgAmount(), 0.005);
        $this->assertEqualsWithDelta($statistic->getAvgTaxRate(), $apiStatistic->getAvgTaxRate(), 0.005);
    }

    public function getData(): array
    {
        return [
            ['2020-01-15', 10, 458971.9872, 4586.632, 23.5898],
            ['2019-12-31', 7, 45878.99, 457.63, 89.64]
        ];
    }
}
