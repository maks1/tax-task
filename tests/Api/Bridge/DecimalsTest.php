<?php

namespace App\Tests\Api\Bridge;

use App\Api\Bridge\Decimals;
use PHPUnit\Framework\TestCase;

class DecimalsTest extends TestCase
{

    /**
     * @dataProvider getData
     */
    public function testRound(float $amount, float $expected)
    {
        $testValue = Decimals::round($amount);

        $this->assertEqualsWithDelta($expected, $testValue, 0.001);
    }

    public function getData(): array
    {
        return [
            [0, 0.0],
            [12.01, 12.01],
            [15.007, 15.01],
            [0.0001, 0.0],
            [99.99997, 100.0]
        ];
    }
}
