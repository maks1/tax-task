const Encore = require('@symfony/webpack-encore');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const path = require('path');

Encore
// directory where all compiled assets will be stored
    .setOutputPath('public/build/')
    // what's the public path to this directory (relative to your project's document root dir)
    .setPublicPath('/build')
    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()
    // will output as web/build/app.js
    .addEntry('app', ['whatwg-fetch', './assets/js/clientSideEntryPoint.js'])
    // will output as web/build/app.css
    .addStyleEntry('css/main',
        [
            './assets/sass/layout.scss',
        ]
    )
    // allow sass/scss files to be processed
    // allow less files to be processed
    .enableLessLoader()
    .enableSassLoader()
    .autoProvideVariables({
        _: 'lodash',
    })
    // enable react in babel
    // .enableReactPreset()
    // Since Encore 4
    .disableSingleRuntimeChunk()
    // create hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())
    .enableSourceMaps(!Encore.isProduction())
    .addPlugin(new CopyWebpackPlugin([{from: './assets/images', to: '../images'}]));
//.configureUglifyJsPlugin();

if (Encore.isProduction()) {
    Encore.addPlugin(new UglifyJsPlugin({
        sourceMap: true,
        extractComments: true,
        parallel: true,
        uglifyOptions: {
            minimize: true,
            output: {comments: false},
            compress: {warnings: false, drop_console: true},
            mangle: {toplevel: true},
        },
        include: /\.min\.js$/
    }));
    Encore.addPlugin(new OptimizeCssAssetsPlugin({
        assetNameRegExp: /\.min\.css$/,
        cssProcessorOptions: {discardComments: {removeAll: true}}
    }))
}

const config = Encore.getWebpackConfig();
config.watchOptions = {poll: true, ignored: /node_modules/};
// export the final configuration
module.exports = config;