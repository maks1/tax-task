<?php

namespace App\Controller;

use App\Action\CountryAction;
use App\Api\Bridge\Country\CountryToApiCountryBridge;
use App\Api\Builder\ApiRequestBuilder;
use App\Api\Builder\ApiResponseBuilder;
use App\Api\Response\ApiCountryListResponse;
use App\Api\Response\ApiCountryResponse;
use App\Api\Response\ApiErrorResponse;
use App\Api\Validator\ApiCountryValidator;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

class ApiCountryController extends ApiController
{
    /** @var CountryAction */
    private $action;

    public function __construct(
        ApiRequestBuilder $apiRequestBuilder,
        ApiResponseBuilder $apiResponseBuilder,
        CountryAction $action
    )
    {
        parent::__construct($apiRequestBuilder, $apiResponseBuilder);
        $this->action = $action;
    }

    /**
     * @Rest\Get("/api/country", options={"expose"=true}, name="api_country_list")
     * @Rest\View(serializerGroups={"Default", "list"})
     * @SWG\Get(path="/api/country", tags={"Dictionaries"}, summary="Get country list", description="Get country list",
     *     @SWG\Response(response=200, description="Success response", @Model(type=ApiCountryListResponse::class, groups={"Default", "list"})),
     *     @SWG\Response(response=500, description="Error response", @Model(type=ApiErrorResponse::class))
     * )
     */
    public function listAction(
        CountryToApiCountryBridge $bridge
    ): View
    {
        $countries = $this->action->list();

        $apiResponse = new ApiCountryListResponse();
        foreach ($countries as $country) {
            $apiResponse->addCountry($bridge->build($country));
        }

        return $this->buildResponse($apiResponse);
    }

    /**
     * @Rest\Get("/api/country/{countryCode}", options={"expose"=true}, name="api_country_info")
     * @Rest\View(serializerGroups={"Default", "info"})
     * @SWG\Get(path="/api/country/{countryCode}", tags={"Dictionaries"}, summary="Get country info", description="Get country",
     *     @SWG\Response(response=200, description="Success response", @Model(type=ApiCountryResponse::class, groups={"Default", "info"})),
     *     @SWG\Response(response=500, description="Error response", @Model(type=ApiErrorResponse::class))
     * )
     */
    public function infoAction(
        string $countryCode,
        ApiCountryValidator $validator,
        CountryToApiCountryBridge $bridge
    ): View
    {
        $validator->validateCountryCode($countryCode);

        $country = $this->action->info($countryCode);

        $apiResponse = new ApiCountryResponse();
        $apiResponse->setCountry($bridge->build($country));

        return $this->buildResponse($apiResponse);
    }
}
