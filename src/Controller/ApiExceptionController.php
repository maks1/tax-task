<?php

namespace App\Controller;

use App\Api\Builder\ApiResponseBuilder;
use App\Api\Entity\ApiError;
use App\Api\Entity\ApiErrorCode;
use App\Api\Exception\ApiBadRequestException;
use App\Api\Exception\ApiException;
use App\Api\Exception\ApiResourceNotFoundException;
use App\Api\Response\ApiErrorResponse;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ApiExceptionController
{
    private static $mapStatusCodes = [
        ApiBadRequestException::class => ApiErrorCode::BAD_REQUEST,
        ApiResourceNotFoundException::class => ApiErrorCode::NOT_FOUND,
    ];

    private static $mapMessages = [
        ApiBadRequestException::class => 'Bad request.',
        ApiResourceNotFoundException::class => 'Resource not found.',
    ];

    private const MESSAGE_TECHNICAL_ERROR = 'Technical error.';

    /** @var ViewHandlerInterface $viewHandler */
    private $viewHandler;

    /** @var ApiResponseBuilder */
    private $restResponseBuilder;

    /** @var bool */
    private $debug;

    public function __construct(ViewHandlerInterface $viewHandler, ApiResponseBuilder $restResponseBuilder, bool $debug)
    {
        $this->viewHandler = $viewHandler;
        $this->restResponseBuilder = $restResponseBuilder;
        $this->debug = $debug;
    }

    public function indexAction(Request $request, \Throwable $exception): Response
    {
        error_log($exception);

        $this->getAndCleanOutputBuffering((int)$request->headers->get('X-Php-Ob-Level', -1));

        $errorCode = $this->getStatusCode($exception);
        $headers = $this->getHeaders($exception);
        $errorMessage = $this->getMessage($exception);

        $apiError = new ApiError();
        $apiError->setCode($errorCode);
        $apiError->setMessage($errorMessage);
        if ($this->debug) {
            $apiError->setDetails($exception->getMessage()."\n".$exception->getTraceAsString());
        }

        $restResponse = new ApiErrorResponse();
        $restResponse->addError($apiError);

        $view = $this->restResponseBuilder->build($restResponse, null, $errorCode, $headers);

        return $this->viewHandler->handle($view);
    }

    private function getAndCleanOutputBuffering(int $startObLevel): string
    {
        if (ob_get_level() <= $startObLevel) {
            return '';
        }
        Response::closeOutputBuffers($startObLevel + 1, true);

        return ob_get_clean();
    }

    private function getStatusCode(\Throwable $exception): int
    {
        $class = get_class($exception);

        if (isset(self::$mapStatusCodes[$class])) {
            return self::$mapStatusCodes[$class];
        }

        if ($exception instanceof HttpExceptionInterface) {
            return $exception->getStatusCode();
        }

        return ApiErrorCode::INTERNAL_SERVER_ERROR;
    }

    private function getHeaders(\Throwable $exception): array
    {
        if ($exception instanceof HttpExceptionInterface) {
            return $exception->getHeaders();
        }

        return [];
    }

    private function getMessage(\Throwable $exception): string
    {
        if ($exception instanceof ApiException) {
            if ($exception->getMessage()) {
                return $exception->getMessage();
            }
        }

        $class = get_class($exception);
        if (array_key_exists($class, self::$mapMessages)) {
            return self::$mapMessages[$class] ?? $exception->getMessage();
        }

        if ($exception instanceof HttpExceptionInterface) {
            if ($exception->getMessage()) {
                return $exception->getMessage();
            }

            $code = $exception->getStatusCode();
            if (array_key_exists($code, Response::$statusTexts)) {
                return Response::$statusTexts[$code];
            }
        }

        return self::MESSAGE_TECHNICAL_ERROR;
    }
}
