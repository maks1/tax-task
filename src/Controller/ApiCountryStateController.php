<?php

namespace App\Controller;

use App\Action\CountryAction;
use App\Api\Bridge\Country\CountryStateToApiCountryStateBridge;
use App\Api\Builder\ApiRequestBuilder;
use App\Api\Builder\ApiResponseBuilder;
use App\Api\Response\ApiCountryStateListResponse;
use App\Api\Response\ApiErrorResponse;
use App\Api\Validator\ApiCountryValidator;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

class ApiCountryStateController extends ApiController
{
    /** @var CountryAction */
    private $action;

    public function __construct(
        ApiRequestBuilder $apiRequestBuilder,
        ApiResponseBuilder $apiResponseBuilder,
        CountryAction $action
    )
    {
        parent::__construct($apiRequestBuilder, $apiResponseBuilder);
        $this->action = $action;
    }

    /**
     * @Rest\Get("/api/country-state/list/{countryCode}", options={"expose"=true}, name="api_country_state_list")
     * @Rest\View(serializerGroups={"Default", "list"})
     * @SWG\Get(path="/api/country-state/list/{countryCode}", tags={"Dictionaries"}, summary="Get country states list", description="Get country states list",
     *     @SWG\Response(response=200, description="Success response", @Model(type=ApiCountryStateListResponse::class, groups={"Default", "list"})),
     *     @SWG\Response(response=500, description="Error response", @Model(type=ApiErrorResponse::class))
     * )
     */
    public function listAction(
        string $countryCode,
        ApiCountryValidator $validator,
        CountryStateToApiCountryStateBridge $bridge
    ): View
    {
        $validator->validateCountryCode($countryCode);
        $country = $this->action->info($countryCode);

        $apiResponse = new ApiCountryStateListResponse();
        foreach ($country->getStates() as $state) {
            $apiResponse->addCountryState($bridge->build($state));
        }

        return $this->buildResponse($apiResponse);
    }
}
