<?php

namespace App\Controller;

use App\Api\Builder\ApiRequestBuilder;
use App\Api\Builder\ApiResponseBuilder;
use App\Api\Request\ApiRequest;
use App\Api\Response\ApiResponse;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends AbstractFOSRestController
{
    /** @var ApiRequestBuilder */
    protected $apiRequestBuilder;

    /** @var ApiResponseBuilder */
    protected $apiResponseBuilder;

    public function __construct(ApiRequestBuilder $apiRequestBuilder, ApiResponseBuilder $apiResponseBuilder)
    {
        $this->apiRequestBuilder = $apiRequestBuilder;
        $this->apiResponseBuilder = $apiResponseBuilder;
    }

    protected function buildRequest(string $requestClass, string $data, ?array $groups = null): ApiRequest
    {
        return $this->apiRequestBuilder->build($requestClass, $data, $groups);
    }

    protected function buildResponse(ApiResponse $response, int $statusCode = null, array $headers = []): View
    {
        if (!$statusCode) {
            $statusCode = $response->isSuccess() ? Response::HTTP_OK : Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return $this->view($response, $statusCode, $headers);
    }

    protected function getFilterFromRequest(Request $request): string
    {
        return json_encode(['filter' => $request->query->all()]);
    }
}
