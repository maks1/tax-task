<?php

namespace App\Controller;

use App\Action\ReportAction;
use App\Api\Bridge\Filter\ApiReportFilterToReportFilterBridge;
use App\Api\Bridge\Statistic\CountryStatisticToApiCountryStatisticBridge;
use App\Api\Builder\ApiRequestBuilder;
use App\Api\Builder\ApiResponseBuilder;
use App\Api\Request\ApiReportRequest;
use App\Api\Response\ApiCountryStatisticResponse;
use App\Api\Response\ApiErrorResponse;
use App\Api\Validator\ApiReportValidator;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;

class ApiReportController extends ApiController
{
    /** @var ReportAction */
    private $action;

    public function __construct(
        ApiRequestBuilder $apiRequestBuilder,
        ApiResponseBuilder $apiResponseBuilder,
        ReportAction $action
    )
    {
        parent::__construct($apiRequestBuilder, $apiResponseBuilder);
        $this->action = $action;
    }

    /**
     * @Rest\Get("/api/report/statistic", options={"expose"=true}, name="report_statistic")
     * @Rest\View(serializerGroups={"Default"})
     * @SWG\Get(path="/api/report/statistic", tags={"Statistic"}, summary="Get statistic of tax collection in country", description="Get statistic of tax collection in country",
     *     @SWG\Parameter(ref="#/parameters/countryCode"),
     *     @SWG\Parameter(ref="#/parameters/period"),
     *     @SWG\Response(response=200, description="Success response", @Model(type=ApiCountryStatisticResponse::class, groups={"Default"})),
     *     @SWG\Response(response=500, description="Error response", @Model(type=ApiErrorResponse::class))
     * )
     */
    public function statisticAction(
        Request $request,
        ApiReportValidator $apiValidator,
        ApiReportFilterToReportFilterBridge $filterBridge,
        CountryStatisticToApiCountryStatisticBridge $bridge
    ): View
    {
        /** @var ApiReportRequest $apiRequest */
        $apiRequest = $this->buildRequest(ApiReportRequest::class, $this->getFilterFromRequest($request));
        $apiValidator->validate($apiRequest);
        $reportFilter = $filterBridge->build($apiRequest->getFilter());

        $countryStatistic = $this->action->getCountryStatistic($reportFilter);

        $apiResponse = new ApiCountryStatisticResponse();
        $apiResponse->setCountryStatistic($bridge->build($countryStatistic));

        return $this->buildResponse($apiResponse);
    }
}
