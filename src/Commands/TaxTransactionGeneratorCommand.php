<?php

namespace App\Commands;

use App\Entity\TaxTransaction;
use App\Repository\CountryRepository;
use App\Repository\TaxTransactionRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TaxTransactionGeneratorCommand extends Command
{
    private const MIN_TRANSACTIONS_COUNT = 60;
    private const MAX_TRANSACTIONS_COUNT = 150;

    protected static $defaultName = 'app:tax-transaction:generate';

    /** @var TaxTransactionRepository */
    private $taxTransactionRepository;

    /** @var CountryRepository */
    private $countryRepository;

    public function __construct(
        TaxTransactionRepository $taxTransactionRepository,
        CountryRepository $countryRepository
    )
    {
        parent::__construct();
        $this->taxTransactionRepository = $taxTransactionRepository;
        $this->countryRepository = $countryRepository;
    }

    protected function configure()
    {
        $this->setDescription('Generate tax transaction by country');
        $this->addArgument('country', InputArgument::REQUIRED, 'Country for generate tax transaction');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln($this->getName() . ' started.');

        $countryCode = trim($input->getArgument('country'));
        $country = $this->countryRepository->find($countryCode);
        if (!$country) {
            throw new \RuntimeException("Country for code $countryCode not found");
        }

        $taxTransactionCollection = [];
        $countTaxTransaction = 0;

        foreach ($country->getStates() as $state) {
            foreach ($state->getCounties() as $county) {
                $output->writeln("Generating transactions for {$state->getTitle()}/{$county->getTitle()}");

                $transactionsCount = rand(self::MIN_TRANSACTIONS_COUNT, self::MAX_TRANSACTIONS_COUNT);

                for ($i = 0; $i < $transactionsCount; $i++) {
                    $contragentName = 'Contragent ' . ($i + 1);
                    $transactionDate = (new \DateTimeImmutable())->modify('-' . rand(0, 6) . ' months');
                    $transactionDate->modify('-' . rand(0, 15) . ' days');
                    $amount = rand(100, 500000) / 100;

                    $taxTransaction = new TaxTransaction($county);
                    $taxTransaction->setDate($transactionDate);
                    $taxTransaction->setContragentName($contragentName);
                    $taxTransaction->setAmount($amount);

                    $taxTransactionCollection[] = $taxTransaction;
                }

                $this->taxTransactionRepository->save($taxTransactionCollection);

                $countTaxTransaction += count($taxTransactionCollection);
                $taxTransactionCollection = [];
            }
        }

        $output->writeln("Added $countTaxTransaction records of tax transactions.");
        $output->writeln($this->getName() . ' finished.');

        return 0;
    }
}
