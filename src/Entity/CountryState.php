<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="country_state",
 *     indexes={
 *         @ORM\Index(name="countryCode", columns={"countryCode"})
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\CountryStateRepository")
 */
class CountryState
{
    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Country
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="countryStates")
     * @ORM\JoinColumn(name="countryCode", referencedColumnName="code", nullable=false, onDelete="CASCADE")
     */
    private $country;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $title = '';

    /**
     * @var Collection|CountryStateCounty[]
     * @ORM\OneToMany(targetEntity="CountryStateCounty", mappedBy="state", cascade={"all"})
     */
    private $counties;

    public function __construct(Country $country)
    {
        $this->country = $country;
        $country->addState($this);
        $this->counties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCountry(): Country
    {
        return $this->country;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return Collection|CountryStateCounty[]
     */
    public function getCounties(): Collection
    {
        return $this->counties;
    }

    public function addCounty(CountryStateCounty $county): void
    {
        if (!$this->getCounties()->contains($county)) {
            $this->getCounties()->add($county);
        }
    }
}