<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="country_state_county",
 *     indexes={
 *         @ORM\Index(name="countryStateId", columns={"countryStateId"})
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\CountryStateCountyRepository")
 */
class CountryStateCounty
{
    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CountryState
     * @ORM\ManyToOne(targetEntity="CountryState", inversedBy="counties")
     * @ORM\JoinColumn(name="countryStateId", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $state;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $title = '';

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=6, scale=2, nullable=false, options={"default"=0, "unsigned"=true})
     */
    private $taxRate = 0.0;

    public function __construct(CountryState $countryState)
    {
        $this->state = $countryState;
        $countryState->addCounty($this);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getState(): CountryState
    {
        return $this->state;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTaxRate(): float
    {
        return $this->taxRate;
    }

    public function setTaxRate(float $taxRate): void
    {
        $this->taxRate = $taxRate;
    }
}