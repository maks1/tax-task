<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="country")
 *
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 */
class Country
{
    /**
     * @var string
     * @ORM\Column(type="string", length=5, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $code;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $title = '';

    /**
     * @var string
     * @ORM\Column(type="string", length=3, nullable=false)
     */
    private $currencyCode;

    /**
     * @var Collection|CountryState[]
     * @ORM\OneToMany(targetEntity="CountryState", mappedBy="country", cascade={"all"})
     */
    private $countryStates;

    public function __construct(string $code, string $currencyCode)
    {
        $this->code = $code;
        $this->currencyCode = $currencyCode;
        $this->countryStates = new ArrayCollection();
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    /**
     * @return Collection|CountryState[]
     */
    public function getStates(): Collection
    {
        return $this->countryStates;
    }

    public function addState(CountryState $countryState): void
    {
        if (!$this->getStates()->contains($countryState)) {
            $this->getStates()->add($countryState);
        }
    }
}