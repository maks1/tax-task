<?php

namespace App\Entity\Statistic;

use App\Entity\Country;

class CountryStatistic
{
    /** @var Country */
    private $country;

    /** @var CountryStateStatistic[] */
    private $stateStatistics = [];

    /** @var Statistic|null */
    private $statistic;

    public function __construct(Country $country)
    {
        $this->country = $country;
    }

    public function getCountry(): Country
    {
        return $this->country;
    }

    public function getStatistic(): ?Statistic
    {
        return $this->statistic;
    }

    public function setStatistic(?Statistic $statistic): void
    {
        $this->statistic = $statistic;
    }

    /**
     * @return CountryStateStatistic[]
     */
    public function getStateStatistics(): array
    {
        return $this->stateStatistics;
    }

    public function addStateStatistic(CountryStateStatistic $stateStatistic): void
    {
        $this->stateStatistics[] = $stateStatistic;
    }

    public function clear(): void
    {
        $this->stateStatistics = [];
        $this->setStatistic(null);
    }
}