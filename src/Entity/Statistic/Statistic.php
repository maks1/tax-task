<?php

namespace App\Entity\Statistic;

class Statistic
{
    /** @var \DateTimeImmutable */
    private $period;

    /** @var int */
    private $countOfCounties = 0;

    /** @var float */
    private $totalAmount = 0.0;

    /** @var float */
    private $avgAmount = 0.0;

    /** @var float */
    private $avgTaxRate = 0.0;

    public function __construct(\DateTimeImmutable $period)
    {
        $this->period = $period;
    }

    public function getPeriod(): \DateTimeImmutable
    {
        return $this->period;
    }

    public function getCountOfCounties(): int
    {
        return $this->countOfCounties;
    }

    public function setCountOfCounties(int $countOfCounties): void
    {
        $this->countOfCounties = $countOfCounties;
    }

    public function getTotalAmount(): float
    {
        return $this->totalAmount;
    }

    public function setTotalAmount(float $totalAmount): void
    {
        $this->totalAmount = $totalAmount;
    }

    public function getAvgAmount(): float
    {
        return $this->avgAmount;
    }

    public function setAvgAmount(float $avgAmount): void
    {
        $this->avgAmount = $avgAmount;
    }

    public function getAvgTaxRate(): float
    {
        return $this->avgTaxRate;
    }

    public function setAvgTaxRate(float $avgTaxRate): void
    {
        $this->avgTaxRate = $avgTaxRate;
    }
}