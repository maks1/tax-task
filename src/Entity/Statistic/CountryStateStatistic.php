<?php

namespace App\Entity\Statistic;

use App\Entity\CountryState;

class CountryStateStatistic
{
    /** @var CountryState */
    private $state;

    /** @var Statistic|null */
    private $statistic;

    public function __construct(CountryState $state)
    {
        $this->state = $state;
    }

    public function getState(): CountryState
    {
        return $this->state;
    }

    public function getStatistic(): ?Statistic
    {
        return $this->statistic;
    }

    public function setStatistic(?Statistic $statistic): void
    {
        $this->statistic = $statistic;
    }
}