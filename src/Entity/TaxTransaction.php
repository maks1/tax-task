<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tax_transaction",
 *     indexes={
 *         @ORM\Index(name="countryStateCountyId", columns={"countryStateCountyId"})
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\TaxTransactionRepository")
 */
class TaxTransaction
{
    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CountryStateCounty
     * @ORM\ManyToOne(targetEntity="CountryStateCounty")
     * @ORM\JoinColumn(name="countryStateCountyId", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $county;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="date_immutable", nullable=false)
     */
    private $date;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $contragentName = '';

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=14, scale=2, nullable=false, options={"default"=0, "unsigned"=true})
     */
    private $amount = 0.0;

    public function __construct(CountryStateCounty $county)
    {
        $this->county = $county;
        $this->date = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCounty(): CountryStateCounty
    {
        return $this->county;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): void
    {
        $this->date = $date;
    }

    public function getContragentName(): string
    {
        return $this->contragentName;
    }

    public function setContragentName(string $contragentName): void
    {
        $this->contragentName = $contragentName;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }
}