<?php

namespace App\Entity\Filter;

use App\Entity\Country;

class ReportFilter
{
    /** @var Country|null */
    private $country;

    /** @var \DateTimeImmutable|null */
    private $period;

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): void
    {
        $this->country = $country;
    }

    public function getPeriod(): ?\DateTimeImmutable
    {
        return $this->period;
    }

    public function setPeriod(?\DateTimeImmutable $period): void
    {
        $this->period = $period;
    }
}
