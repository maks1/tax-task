<?php

namespace App\Action;

use App\Entity\CountryState;
use App\Entity\Filter\ReportFilter;
use App\Entity\Statistic\CountryStateStatistic;
use App\Entity\Statistic\CountryStatistic;
use App\Entity\Statistic\Statistic;
use App\Repository\CountryStateCountyRepository;
use App\Repository\TaxTransactionRepository;
use App\Service\Cache\CacheService;

class ReportAction
{
    /** @var TaxTransactionRepository */
    private $taxTransactionRepository;

    /** @var CountryStateCountyRepository */
    private $countyRepository;

    /** @var CacheService */
    private $cacheService;

    public function __construct(
        TaxTransactionRepository $taxTransactionRepository,
        CountryStateCountyRepository $countyRepository,
        CacheService $cacheService
    )
    {
        $this->taxTransactionRepository = $taxTransactionRepository;
        $this->countyRepository = $countyRepository;
        $this->cacheService = $cacheService;
    }

    public function getCountryStatistic(ReportFilter $filter): CountryStatistic
    {
        $country = $filter->getCountry();
        $period = $filter->getPeriod();

        $countryStatistic = new CountryStatistic($country);

        $statistics = [];
        foreach ($country->getStates() as $state) {
            $statistic = $this->getCountryStateStatistic($period, $state);

            $stateStatistic = new CountryStateStatistic($state);
            $stateStatistic->setStatistic($statistic);
            $countryStatistic->addStateStatistic($stateStatistic);

            $statistics[] = $statistic;
        }

        $totalStatistic = $this->getTotalStatistic($period, $statistics);
        $countryStatistic->setStatistic($totalStatistic);

        return $countryStatistic;
    }

    private function getCountryStateStatistic(\DateTimeImmutable $period, CountryState $state): Statistic
    {
        // Cache statistic of one country state.
        $cacheKey = 'state-stat-'.$period->format('Ym').'-'.$state->getId();

        /** @var Statistic $statistic */
        $statistic = $this->cacheService->get($cacheKey, function () use ($state, $period) {
            $countOfCounties = $state->getCounties()->count();
            $totalAmount = $this->taxTransactionRepository->getOverallAmountByState($state, $period);
            $avgAmount = $this->taxTransactionRepository->getAverageAmountByState($state, $period);
            $avgTaxRate = $this->countyRepository->getAverageTaxRateByState($state);

            $statistic = new Statistic($period);
            $statistic->setCountOfCounties($countOfCounties);
            $statistic->setTotalAmount($totalAmount);
            $statistic->setAvgAmount($avgAmount);
            $statistic->setAvgTaxRate($avgTaxRate);

            return $statistic;
        });

        return $statistic;
    }

    /**
     * @param Statistic[] $statistics
     */
    private function getTotalStatistic(\DateTimeImmutable $period, array $statistics): Statistic
    {
        $totalCountOfCounties = 0;
        $totalAmount = 0;
        $totalTaxRate = 0;
        foreach ($statistics as $statistic) {
            $totalCountOfCounties += $statistic->getCountOfCounties();
            $totalAmount += $statistic->getTotalAmount();
            $totalTaxRate += $statistic->getAvgTaxRate() * $statistic->getCountOfCounties();
        }
        $totalAvgAmount = $totalCountOfCounties > 0 ? $totalAmount / $totalCountOfCounties : 0;
        $totalAvgTaxRate = $totalCountOfCounties > 0 ? $totalTaxRate / $totalCountOfCounties : 0;

        $totalStatistic = new Statistic($period);

        $totalStatistic->setCountOfCounties($totalCountOfCounties);
        $totalStatistic->setTotalAmount($totalAmount);
        $totalStatistic->setAvgAmount($totalAvgAmount);
        $totalStatistic->setAvgTaxRate($totalAvgTaxRate);

        return $totalStatistic;
    }
}
