<?php

namespace App\Action;

use App\Entity\Country;
use App\Repository\CountryRepository;

class CountryAction
{
    /** @var CountryRepository */
    private $countryRepository;

    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    public function info(string $code): ?Country
    {
        return $this->countryRepository->find($code);
    }

    public function list(): ?array
    {
        return $this->countryRepository->findAll();
    }
}
