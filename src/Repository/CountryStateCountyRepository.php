<?php

namespace App\Repository;

use App\Entity\CountryState;
use App\Entity\CountryStateCounty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CountryStateCountyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CountryStateCounty::class);
    }

    public function find($id, $lockMode = null, $lockVersion = null): ?CountryStateCounty
    {
        /** @var CountryStateCounty $entity */
        $entity = $id ? parent::find($id, $lockMode, $lockVersion) : null;

        return $entity;
    }

    public function getAverageTaxRateByState(CountryState $state): float
    {
        $qb = $this->createQueryBuilder('ct');
        $qb->select('AVG(ct.taxRate)');
        $qb->andWhere('ct.state = :state')->setParameter('state', $state);

        return (float)$qb->getQuery()->getSingleScalarResult();
    }
}
