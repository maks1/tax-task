<?php

namespace App\Repository;

use App\Entity\CountryState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CountryStateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CountryState::class);
    }

    public function find($id, $lockMode = null, $lockVersion = null): ?CountryState
    {
        /** @var CountryState $entity */
        $entity = $id ? parent::find($id, $lockMode, $lockVersion) : null;

        return $entity;
    }
}
