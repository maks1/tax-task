<?php

namespace App\Repository;

use App\Entity\CountryState;
use App\Entity\TaxTransaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TaxTransactionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaxTransaction::class);
    }

    public function find($id, $lockMode = null, $lockVersion = null): ?TaxTransaction
    {
        /** @var TaxTransaction $entity */
        $entity = $id ? parent::find($id, $lockMode, $lockVersion) : null;

        return $entity;
    }

    public function getOverallAmountByState(CountryState $state, \DateTimeImmutable $period): float
    {
        $periodStart = $this->getPeriodStart($period);
        $periodEnd = $this->getPeriodEnd($period);

        $qb = $this->createQueryBuilder('tt');
        $qb->select('SUM(tt.amount)');
        $qb->innerJoin('tt.county', 'ct');
        $qb->andWhere('ct.state = :state')->setParameter('state', $state);
        $qb->andWhere('tt.date >= :periodStart')->setParameter('periodStart', $periodStart);
        $qb->andWhere('tt.date <= :periodEnd')->setParameter('periodEnd', $periodEnd);

        return (float)$qb->getQuery()->getSingleScalarResult();
    }

    public function getAverageAmountByState(CountryState $state, \DateTimeImmutable $period): float
    {
        $periodStart = $this->getPeriodStart($period);
        $periodEnd = $this->getPeriodEnd($period);

        $qb = $this->createQueryBuilder('tt');
        $qb->select('AVG(tt.amount)');
        $qb->innerJoin('tt.county', 'ct');
        $qb->andWhere('ct.state = :state')->setParameter('state', $state);
        $qb->andWhere('tt.date >= :periodStart')->setParameter('periodStart', $periodStart);
        $qb->andWhere('tt.date <= :periodEnd')->setParameter('periodEnd', $periodEnd);

        return (float)$qb->getQuery()->getSingleScalarResult();
    }

    private function getPeriodStart(\DateTimeImmutable $period): \DateTimeImmutable
    {
        return new \DateTimeImmutable($period->format('Y-m-01'));
    }

    private function getPeriodEnd(\DateTimeImmutable $period): \DateTimeImmutable
    {
        return new \DateTimeImmutable($period->format('Y-m-t'));
    }

    public function save(array $taxTransactions): void
    {
        foreach ($taxTransactions as $taxTransaction) {
            $this->getEntityManager()->persist($taxTransaction);
        }
        $this->getEntityManager()->flush($taxTransactions);
    }
}
