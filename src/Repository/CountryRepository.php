<?php

namespace App\Repository;

use App\Entity\Country;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CountryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Country::class);
    }

    public function find($code, $lockMode = null, $lockVersion = null): ?Country
    {
        /** @var Country $entity */
        $entity = $code ? parent::find($code, $lockMode, $lockVersion) : null;

        return $entity;
    }
}
