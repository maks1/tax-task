<?php

namespace App\Service\Cache;

use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class CacheService
{
    private const EXPIRES_AFTER = 3600;

    /** @var CacheInterface */
    private $cache;

    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    public function get(string $key, callable $callback)
    {
        return $this->cache->get($key, function (ItemInterface $item) use ($callback) {
            $data = $callback();
            $item->expiresAfter(self::EXPIRES_AFTER);

            return $data;
        });
    }
}