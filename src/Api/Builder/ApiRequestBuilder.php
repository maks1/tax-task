<?php

namespace App\Api\Builder;

use App\Api\Exception\ApiBadRequestException;
use App\Api\Request\ApiRequest;
use JMS\Serializer\ContextFactory\DeserializationContextFactoryInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializerInterface;

class ApiRequestBuilder
{
    /** @var SerializerInterface */
    private $serializer;

    /** @var DeserializationContextFactoryInterface */
    private $deserializationContextFactory;

    public function __construct(
        SerializerInterface $serializer,
        DeserializationContextFactoryInterface $deserializationContextFactory
    )
    {
        $this->serializer = $serializer;
        $this->deserializationContextFactory = $deserializationContextFactory;
    }

    public function build(string $requestClass, string $data, ?array $groups = null): ApiRequest
    {
        if (!$data) {
            $data = '{}'; // Empty request.
        }

        try {
            /** @var ApiRequest $request */
            $request = $this->serializer->deserialize($data, $requestClass, 'json', $this->getDeserializationContext($groups));
            if ($requestClass !== get_class($request) || (!$request instanceof ApiRequest)) {
                throw new \LogicException('Wrong request class name: ' . $requestClass);
            }

            return $request;
        } catch (\Throwable $e) {
            throw new ApiBadRequestException($e->getMessage(), $e->getCode(), $e);
        }
    }

    private function getDeserializationContext(?array $groups): ?DeserializationContext
    {
        if ($groups) {
            if (!in_array('Default', $groups, true)) {
                $groups[] = 'Default';
            }
            $context = $this->deserializationContextFactory->createDeserializationContext();
            $context->setGroups($groups);
        } else {
            $context = null;
        }

        return $context;
    }
}
