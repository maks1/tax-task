<?php

namespace App\Api\Builder;

use App\Api\Response\ApiResponse;
use FOS\RestBundle\View\View;
use JMS\Serializer\ArrayTransformerInterface;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;

class ApiResponseBuilder
{
    /** @var ArrayTransformerInterface $arrayTransformer */
    private $arrayTransformer;

    public function __construct(ArrayTransformerInterface $arrayTransformer)
    {
        $this->arrayTransformer = $arrayTransformer;
    }

    public function build(ApiResponse $response, SerializationContext $context = null, int $statusCode = null, array $headers = []): View
    {
        $data = $this->arrayTransformer->toArray($response, $context);
        if (!$statusCode) {
            $statusCode = $response->isSuccess() ? Response::HTTP_OK : Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return View::create($data, $statusCode, $headers);
    }
}
