<?php

namespace App\Api\Validator\Assert;

use App\Api\Exception\ApiBadRequestException;
use App\Api\Exception\ApiResourceNotFoundException;
use App\Repository\CountryRepository;

class CountryCodeAssert implements AssertInterface
{
    /** @var CountryRepository */
    private $countryRepository;

    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    public function validate($value): void
    {
        if (!trim($value)) {
            throw new ApiBadRequestException('Wrong {countryCode}.');
        }
        if (!$this->countryRepository->find($value)) {
            throw new ApiResourceNotFoundException("Country with code: $value is not found.");
        }
    }
}