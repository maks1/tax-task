<?php

namespace App\Api\Validator\Assert;

use App\Api\Exception\ApiException;

interface AssertInterface
{
    /**
     * @param mixed $value
     * @throws ApiException
     */
    public function validate($value): void;
}