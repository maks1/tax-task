<?php

namespace App\Api\Validator;

use App\Api\Validator\Assert\CountryCodeAssert;

class ApiCountryValidator
{
    /** @var CountryCodeAssert */
    private $countryCodeAssert;

    public function __construct(CountryCodeAssert $countryCodeAssert)
    {
        $this->countryCodeAssert = $countryCodeAssert;
    }

    public function validateCountryCode(string $countryCode): void
    {
        $this->countryCodeAssert->validate($countryCode);
    }
}