<?php


namespace App\Api\Validator;

use App\Api\Exception\ApiBadRequestException;
use App\Api\Request\ApiReportRequest;
use App\Api\Validator\Assert\CountryCodeAssert;

class ApiReportValidator
{
    /** @var CountryCodeAssert */
    private $countryCodeAssert;

    public function __construct(CountryCodeAssert $countryCodeAssert)
    {
        $this->countryCodeAssert = $countryCodeAssert;
    }

    public function validate(ApiReportRequest $request): void
    {
        $filter = $request->getFilter();
        if (!$filter) {
            throw new ApiBadRequestException('Filter is required.');
        }
        if (!$filter->getPeriod()) {
            throw new ApiBadRequestException('Period is required.');
        }

        $this->countryCodeAssert->validate($filter->getCountryCode());
    }
}