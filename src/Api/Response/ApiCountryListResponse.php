<?php

namespace App\Api\Response;

use App\Api\Entity\ApiCountry;
use JMS\Serializer\Annotation as JMS;

class ApiCountryListResponse extends ApiResponse
{
    /**
     * @var ApiCountry[]
     * @JMS\Type("array<App\Api\Entity\ApiCountry>")
     */
    protected $list = [];

    /**
     * @return ApiCountry[]
     */
    public function getCountries(): array
    {
        return $this->list;
    }

    public function addCountry(ApiCountry $country): void
    {
        $this->list[] = $country;
    }
}
