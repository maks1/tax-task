<?php

namespace App\Api\Response;

use App\Api\Entity\ApiCountryState;
use JMS\Serializer\Annotation as JMS;

class ApiCountryStateListResponse extends ApiResponse
{
    /**
     * @var ApiCountryState[]
     * @JMS\Type("array<App\Api\Entity\ApiCountryState>")
     */
    protected $list = [];

    /**
     * @return ApiCountryState[]
     */
    public function getCountryStates(): array
    {
        return $this->list;
    }

    public function addCountryState(ApiCountryState $countryState): void
    {
        $this->list[] = $countryState;
    }
}
