<?php

namespace App\Api\Response;

use App\Api\Entity\ApiCountry;
use JMS\Serializer\Annotation as JMS;

class ApiCountryResponse extends ApiResponse
{
    /**
     * @var ApiCountry|null
     * @JMS\Type("App\Api\Entity\ApiCountry")
     */
    protected $country;

    public function getCountry(): ?ApiCountry
    {
        return $this->country;
    }

    public function setCountry(?ApiCountry $country): void
    {
        $this->country = $country;
    }
}
