<?php

namespace App\Api\Response;

use JMS\Serializer\Annotation as JMS;

abstract class ApiResponse
{
    const VERSION = '1.0';

    /**
     * @var boolean
     * @JMS\Type("boolean")
     * @JMS\ReadOnly()
     */
    private $success = true;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\ReadOnly()
     */
    private $version = self::VERSION;

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }

    public function getVersion(): string
    {
        return $this->version;
    }
}
