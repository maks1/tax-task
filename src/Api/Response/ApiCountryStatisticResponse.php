<?php

namespace App\Api\Response;

use App\Api\Entity\ApiCountryStatistic;
use JMS\Serializer\Annotation as JMS;

class ApiCountryStatisticResponse extends ApiResponse
{
    /**
     * @var ApiCountryStatistic|null
     * @JMS\Type("App\Api\Entity\ApiCountryStatistic")
     */
    protected $countryStatistic;

    public function getCountryStatistic(): ?ApiCountryStatistic
    {
        return $this->countryStatistic;
    }

    public function setCountryStatistic(?ApiCountryStatistic $countryStatistic): void
    {
        $this->countryStatistic = $countryStatistic;
    }
}
