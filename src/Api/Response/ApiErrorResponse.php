<?php

namespace App\Api\Response;

use App\Api\Entity\ApiError;
use JMS\Serializer\Annotation as JMS;

class ApiErrorResponse extends ApiResponse
{
    /**
     * @var ApiError[]
     * @JMS\Type("array<App\Api\Entity\ApiError>")
     * @JMS\ReadOnly()
     */
    protected $errors = [];

    public function __construct()
    {
        $this->setSuccess(false);
    }

    /**
     * @return ApiError[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function addError(ApiError $error): void
    {
        $this->errors[] = $error;
    }
}
