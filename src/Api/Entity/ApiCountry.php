<?php

namespace App\Api\Entity;

use JMS\Serializer\Annotation as JMS;

class ApiCountry
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\Groups({"info", "list"})
     * @JMS\ReadOnly()
     */
    protected $code;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\Groups({"info", "list"})
     * @JMS\ReadOnly()
     */
    protected $title;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\Groups({"info", "list"})
     * @JMS\ReadOnly()
     */
    protected $currencyCode;

    /**
     * @var ApiCountryState[]
     * @JMS\Type("array<App\Api\Entity\ApiCountryState>")
     * @JMS\Groups({"info"})
     * @JMS\ReadOnly()
     */
    protected $states;

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(?string $currencyCode): void
    {
        $this->currencyCode = $currencyCode;
    }

    /**
     * @return ApiCountryState[]|null
     */
    public function getStates(): ?array
    {
        return $this->states;
    }

    public function addState(ApiCountryState $state): void
    {
        $this->states[] = $state;
    }
}
