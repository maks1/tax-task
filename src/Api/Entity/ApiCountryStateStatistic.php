<?php

namespace App\Api\Entity;

use JMS\Serializer\Annotation as JMS;

class ApiCountryStateStatistic
{
    /**
     * @var int
     * @JMS\Type("integer")
     * @JMS\ReadOnly()
     */
    protected $stateId;

    /**
     * @var ApiStatistic
     * @JMS\Type("App\Api\Entity\ApiStatistic")
     * @JMS\ReadOnly()
     */
    protected $statistic;

    public function getStateId(): ?int
    {
        return $this->stateId;
    }

    public function setStateId(?int $stateId): void
    {
        $this->stateId = $stateId;
    }

    public function getStatistic(): ?ApiStatistic
    {
        return $this->statistic;
    }

    public function setStatistic(?ApiStatistic $statistic): void
    {
        $this->statistic = $statistic;
    }
}
