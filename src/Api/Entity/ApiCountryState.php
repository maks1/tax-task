<?php

namespace App\Api\Entity;

use JMS\Serializer\Annotation as JMS;

class ApiCountryState
{
    /**
     * @var int
     * @JMS\Type("integer")
     * @JMS\Groups({"info", "list"})
     * @JMS\ReadOnly()
     */
    protected $id;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\Groups({"info", "list"})
     * @JMS\ReadOnly()
     */
    protected $title;

    /**
     * @var ApiCountryStateCounty[]
     * @JMS\Type("array<App\Api\Entity\ApiCountryStateCounty>")
     * @JMS\Groups({"info"})
     * @JMS\ReadOnly()
     */
    protected $counties;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return ApiCountryStateCounty[]|null
     */
    public function getCounties(): ?array
    {
        return $this->counties;
    }

    public function addCounty(ApiCountryStateCounty $county): void
    {
        $this->counties[] = $county;
    }
}
