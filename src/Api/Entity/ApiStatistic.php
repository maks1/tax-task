<?php

namespace App\Api\Entity;

use JMS\Serializer\Annotation as JMS;

class ApiStatistic
{
    /**
     * @var int
     * @JMS\Type("integer")
     * @JMS\ReadOnly()
     */
    protected $countOfCounties;

    /**
     * @var float
     * @JMS\Type("float")
     * @JMS\ReadOnly()
     */
    protected $totalAmount;

    /**
     * @var float
     * @JMS\Type("float")
     * @JMS\ReadOnly()
     */
    protected $avgAmount;

    /**
     * @var float
     * @JMS\Type("float")
     * @JMS\ReadOnly()
     */
    protected $avgTaxRate;

    public function getCountOfCounties(): ?int
    {
        return $this->countOfCounties;
    }

    public function setCountOfCounties(?int $countOfCounties): void
    {
        $this->countOfCounties = $countOfCounties;
    }

    public function getTotalAmount(): ?float
    {
        return $this->totalAmount;
    }

    public function setTotalAmount(?float $totalAmount): void
    {
        $this->totalAmount = $totalAmount;
    }

    public function getAvgAmount(): ?float
    {
        return $this->avgAmount;
    }

    public function setAvgAmount(?float $avgAmount): void
    {
        $this->avgAmount = $avgAmount;
    }

    public function getAvgTaxRate(): ?float
    {
        return $this->avgTaxRate;
    }

    public function setAvgTaxRate(?float $avgTaxRate): void
    {
        $this->avgTaxRate = $avgTaxRate;
    }
}
