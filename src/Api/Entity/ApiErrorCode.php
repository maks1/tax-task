<?php

namespace App\Api\Entity;

abstract class ApiErrorCode
{
    // Use only 4xx, 5xx error codes here.
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;

    const INTERNAL_SERVER_ERROR = 500;
}