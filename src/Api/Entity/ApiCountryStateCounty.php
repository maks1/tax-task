<?php

namespace App\Api\Entity;

use JMS\Serializer\Annotation as JMS;

class ApiCountryStateCounty
{
    /**
     * @var int
     * @JMS\Type("integer")
     * @JMS\Groups({"list", "info"})
     * @JMS\ReadOnly()
     */
    protected $id;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\Groups({"list", "info"})
     * @JMS\ReadOnly()
     */
    protected $title;

    /**
     * @var float
     * @JMS\Type("float")
     * @JMS\Groups({"list", "info"})
     * @JMS\ReadOnly()
     */
    protected $taxRate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getTaxRate(): ?float
    {
        return $this->taxRate;
    }

    public function setTaxRate(?float $taxRate): void
    {
        $this->taxRate = $taxRate;
    }
}
