<?php

namespace App\Api\Entity;

use JMS\Serializer\Annotation as JMS;

class ApiError
{
    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\ReadOnly()
     */
    private $code = 0;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\ReadOnly()
     */
    private $message = '';

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\ReadOnly()
     */
    private $details = '';

    public function getCode(): int
    {
        return $this->code;
    }

    public function setCode(int $code): void
    {
        $this->code = $code;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function getDetails(): string
    {
        return $this->details;
    }

    public function setDetails(string $details): void
    {
        $this->details = $details;
    }
}
