<?php

namespace App\Api\Entity;

use JMS\Serializer\Annotation as JMS;

class ApiCountryStatistic
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\ReadOnly()
     */
    protected $countryCode;

    /**
     * @var \DateTimeImmutable
     * @JMS\Type("DateTimeImmutable<'Y-m'>")
     */
    protected $period;

    /**
     * @var ApiStatistic
     * @JMS\Type("App\Api\Entity\ApiStatistic")
     * @JMS\ReadOnly()
     */
    protected $statistic;

    /**
     * @var ApiCountryStateStatistic[]
     * @JMS\Type("array<App\Api\Entity\ApiCountryStateStatistic>")
     * @JMS\ReadOnly()
     */
    protected $stateStatistics;

    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    public function setCountryCode(?string $countryCode): void
    {
        $this->countryCode = $countryCode;
    }

    public function getPeriod(): ?\DateTimeImmutable
    {
        return $this->period;
    }

    public function setPeriod(?\DateTimeImmutable $period): void
    {
        $this->period = $period;
    }

    public function getStatistic(): ?ApiStatistic
    {
        return $this->statistic;
    }

    public function setStatistic(?ApiStatistic $statistic): void
    {
        $this->statistic = $statistic;
    }

    public function getStateStatistics(): array
    {
        return $this->stateStatistics;
    }

    public function addStateStatistic(ApiCountryStateStatistic $apiCountryStateStatistic): void
    {
        $this->stateStatistics[] = $apiCountryStateStatistic;
    }
}
