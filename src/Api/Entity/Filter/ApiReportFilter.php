<?php

namespace App\Api\Entity\Filter;

use JMS\Serializer\Annotation as JMS;

class ApiReportFilter
{
    /**
     * @var string
     * @JMS\Type("string")
     */
    protected $countryCode;

    /**
     * @var \DateTimeImmutable
     * @JMS\Type("DateTimeImmutable<'Y-m-d'>")
     */
    protected $period;

    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    public function getPeriod(): ?\DateTimeImmutable
    {
        return $this->period;
    }
}