<?php

namespace App\Api\Request;

use App\Api\Entity\Filter\ApiReportFilter;
use JMS\Serializer\Annotation\Type;

class ApiReportRequest extends ApiRequest
{
    /**
     * @var ApiReportFilter
     * @Type("App\Api\Entity\Filter\ApiReportFilter")
     */
    protected $filter;

    public function getFilter(): ?ApiReportFilter
    {
        return $this->filter;
    }
}
