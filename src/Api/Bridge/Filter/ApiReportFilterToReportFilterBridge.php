<?php

namespace App\Api\Bridge\Filter;

use App\Api\Entity\Filter\ApiReportFilter;
use App\Entity\Filter\ReportFilter;
use App\Repository\CountryRepository;

class ApiReportFilterToReportFilterBridge
{
    /** @var CountryRepository */
    private $countryRepository;

    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    public function build(ApiReportFilter $apiReportFilter): ReportFilter
    {
        $reportFilter = new ReportFilter();

        $reportFilter->setCountry($this->countryRepository->find($apiReportFilter->getCountryCode()));
        $reportFilter->setPeriod($apiReportFilter->getPeriod());

        return $reportFilter;
    }
}