<?php

namespace App\Api\Bridge\Statistic;

use App\Api\Entity\ApiCountryStateStatistic;
use App\Entity\Statistic\CountryStateStatistic;

class CountryStateStatisticToApiCountryStateStatisticBridge
{
    /** @var StatisticToApiStatisticBridge */
    private $statisticBridge;

    public function __construct(
        StatisticToApiStatisticBridge $statisticBridge
    )
    {
        $this->statisticBridge = $statisticBridge;
    }

    public function build(CountryStateStatistic $countryStateStatistic): ApiCountryStateStatistic
    {
        $apiCountryStatistic = new ApiCountryStateStatistic();

        $apiCountryStatistic->setStateId($countryStateStatistic->getState()->getId());
        $apiCountryStatistic->setStatistic($this->statisticBridge->build($countryStateStatistic->getStatistic()));

        return $apiCountryStatistic;
    }
}
