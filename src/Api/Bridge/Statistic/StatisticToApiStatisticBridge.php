<?php

namespace App\Api\Bridge\Statistic;

use App\Api\Bridge\Decimals;
use App\Api\Entity\ApiStatistic;
use App\Entity\Statistic\Statistic;

class StatisticToApiStatisticBridge
{
    public function build(Statistic $statistic): ApiStatistic
    {
        $apiStatistic = new ApiStatistic();

        $apiStatistic->setCountOfCounties($statistic->getCountOfCounties());
        $apiStatistic->setTotalAmount(Decimals::round($statistic->getTotalAmount()));
        $apiStatistic->setAvgAmount(Decimals::round($statistic->getAvgAmount()));
        $apiStatistic->setAvgTaxRate(Decimals::round($statistic->getAvgTaxRate()));

        return $apiStatistic;
    }
}
