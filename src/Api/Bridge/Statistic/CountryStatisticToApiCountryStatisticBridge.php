<?php

namespace App\Api\Bridge\Statistic;

use App\Api\Entity\ApiCountryStatistic;
use App\Entity\Statistic\CountryStatistic;

class CountryStatisticToApiCountryStatisticBridge
{
    /** @var CountryStateStatisticToApiCountryStateStatisticBridge */
    private $countryStateStatisticBridge;

    /** @var StatisticToApiStatisticBridge */
    private $statisticBridge;

    public function __construct(
        CountryStateStatisticToApiCountryStateStatisticBridge $countryStateStatisticBridge,
        StatisticToApiStatisticBridge $statisticBridge
    )
    {
        $this->countryStateStatisticBridge = $countryStateStatisticBridge;
        $this->statisticBridge = $statisticBridge;
    }

    public function build(CountryStatistic $countryStatistic): ApiCountryStatistic
    {
        $apiCountryStatistic = new ApiCountryStatistic();

        $apiCountryStatistic->setCountryCode($countryStatistic->getCountry()->getCode());
        $apiCountryStatistic->setPeriod($countryStatistic->getStatistic()->getPeriod());
        $apiCountryStatistic->setStatistic($this->statisticBridge->build($countryStatistic->getStatistic()));

        foreach ($countryStatistic->getStateStatistics() as $stateStatistic) {
            $apiCountryStatistic->addStateStatistic($this->countryStateStatisticBridge->build($stateStatistic));
        }
        return $apiCountryStatistic;
    }
}
