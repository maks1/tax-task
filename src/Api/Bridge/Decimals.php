<?php

namespace App\Api\Bridge;

abstract class Decimals
{
    private const DECIMALS = 2;

    public static function round(float $amount): float
    {
        return round($amount, self::DECIMALS);
    }
}