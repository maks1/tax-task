<?php

namespace App\Api\Bridge\Country;

use App\Api\Entity\ApiCountry;
use App\Entity\Country;

class CountryToApiCountryBridge
{
    /** @var CountryStateToApiCountryStateBridge */
    private $stateBridge;

    public function __construct(CountryStateToApiCountryStateBridge $stateBridge)
    {
        $this->stateBridge = $stateBridge;
    }

    public function build(Country $country): ApiCountry
    {
        $apiCountry = new ApiCountry();

        $apiCountry->setCode($country->getCode());
        $apiCountry->setTitle($country->getTitle());
        $apiCountry->setCurrencyCode($country->getCurrencyCode());

        foreach ($country->getStates() as $state) {
            $apiCountry->addState($this->stateBridge->build($state));
        }
        return $apiCountry;
    }
}
