<?php

namespace App\Api\Bridge\Country;

use App\Api\Entity\ApiCountryState;
use App\Entity\CountryState;

class CountryStateToApiCountryStateBridge
{
    /** @var CountryStateCountyToApiCountryStateCountyBridge */
    private $countyBridge;

    public function __construct(CountryStateCountyToApiCountryStateCountyBridge $countyBridge)
    {
        $this->countyBridge = $countyBridge;
    }

    public function build(CountryState $state): ApiCountryState
    {
        $apiState = new ApiCountryState();

        $apiState->setId($state->getId());
        $apiState->setTitle($state->getTitle());

        foreach ($state->getCounties() as $county) {
            $apiState->addCounty($this->countyBridge->build($county));
        }

        return $apiState;
    }
}
