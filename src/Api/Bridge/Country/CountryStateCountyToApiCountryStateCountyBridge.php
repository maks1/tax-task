<?php

namespace App\Api\Bridge\Country;

use App\Api\Bridge\Decimals;
use App\Api\Entity\ApiCountryStateCounty;
use App\Entity\CountryStateCounty;

class CountryStateCountyToApiCountryStateCountyBridge
{
    public function build(CountryStateCounty $county): ApiCountryStateCounty
    {
        $apiCounty = new ApiCountryStateCounty();

        $apiCounty->setId($county->getId());
        $apiCounty->setTitle($county->getTitle());
        $apiCounty->setTaxRate(Decimals::round($county->getTaxRate()));

        return $apiCounty;
    }
}
