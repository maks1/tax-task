<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200416175744 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql("
            INSERT INTO `country_state` (`title`, `countryCode`) 
                VALUES 
                        ('Bavaria', 'DE'),
                        ('Brandenburg', 'DE'),
                        ('North Rhine-Westphalia', 'DE'),
                        ('Saxony', 'DE'),
                        ('Hesse', 'DE')
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("
            DELETE FROM `country_state`
                WHERE `countryCode` = 'DE'
        ");
    }
}
