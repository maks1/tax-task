<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200416155243 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql("
            CREATE TABLE `country` (
                `code` VARCHAR(5) NOT NULL, 
                `title` VARCHAR(255) NOT NULL, 
                `currencyCode` VARCHAR(3) NOT NULL, 
                PRIMARY KEY(`code`)
            ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;
        ");

        $this->addSql("
            CREATE TABLE `country_state` (
                `id` INT AUTO_INCREMENT NOT NULL, 
                `countryCode` VARCHAR(5) NOT NULL,
                `title` VARCHAR(255) NOT NULL, 
                INDEX `countryCode` (`countryCode`), 
                PRIMARY KEY(`id`)
            ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;
        ");

        $this->addSql("
            CREATE TABLE `country_state_county` (
                `id` INT AUTO_INCREMENT NOT NULL, 
                `countryStateId` INT NOT NULL,
                `title` VARCHAR(255) NOT NULL, 
                `taxRate` NUMERIC(6, 2) UNSIGNED DEFAULT 0 NOT NULL, 
                INDEX `countryStateId` (`countryStateId`), 
                PRIMARY KEY(`id`)
            ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;
        ");

        $this->addSql("
            CREATE TABLE `tax_transaction` (
                `id` INT AUTO_INCREMENT NOT NULL, 
                `countryStateCountyId` INT NOT NULL,
                `date` DATE NOT NULL COMMENT '(DC2Type:date_immutable)', 
                `contragentName` VARCHAR(255) NOT NULL, 
                `amount` NUMERIC(14, 2) UNSIGNED DEFAULT 0 NOT NULL, 
                INDEX `countryStateCountyId` (`countryStateCountyId`), 
                PRIMARY KEY(`id`)
            ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;
        ");

        $this->addSql("
            ALTER TABLE `country_state` 
                ADD CONSTRAINT FK_B8440600A164B0CD FOREIGN KEY (`countryCode`) 
                REFERENCES `country` (`code`) 
                ON DELETE CASCADE;
        ");

        $this->addSql("
            ALTER TABLE `country_state_county` 
                ADD CONSTRAINT FK_5273D741C3DE5E83 FOREIGN KEY (`countryStateId`) 
                REFERENCES `country_state` (`id`) 
                ON DELETE CASCADE;
        ");

        $this->addSql("
            ALTER TABLE `tax_transaction` 
                ADD CONSTRAINT FK_8113F27AEF4C247A FOREIGN KEY (`countryStateCountyId`) 
                REFERENCES `country_state_county` (`id`) 
                ON DELETE CASCADE;
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("
            DROP TABLE `tax_transaction`;
        ");

        $this->addSql("
            DROP TABLE `country_state_county`;
        ");

        $this->addSql("
            DROP TABLE `country_state`;
        ");

        $this->addSql("
            DROP TABLE `country`;
        ");
    }
}
