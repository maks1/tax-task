<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200416171416 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql("
            INSERT INTO `country` 
                SET `code` = 'DE', 
                    `title` = 'Germany', 
                    `currencyCode` = 'EUR'
        ");
    }

    public function down(Schema $schema): void
    {
       $this->addSql("
            DELETE FROM `country`
                WHERE `code` = 'DE'
       ");
    }
}
