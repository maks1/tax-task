<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200416180633 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $counties = [
            'Bavaria' => [
                'Lower Bavaria',
                'Lower Franconia' .
                'Upper Franconia',
                'Middle Franconia',
                'Upper Palatinate',
                'Swabia',
                'Upper Bavaria'
            ],
            'Brandenburg' => [
                'Barnim',
                'Dahme-Spreewald',
                'Elbe-Elster',
                'Havelland',
                'Märkisch-Oderland',
                'Oberhavel',
                'Oberspreewald-Lausitz',
                'Oder-Spree',
                'Ostprignitz-Ruppin',
                'Potsdam-Mittelmark',
                'Prignitz',
                'Spree-Neiße',
                'Teltow-Fläming',
                'Uckermark',
                'Stadt Brandenburg an der Havel',
                'Stadt Cottbus',
                'Stadt. Frankfurt (Oder)',
                'Stadt Potsdam'
            ],
            'North Rhine-Westphalia' => [
                'Aachen',
                'Bielefeld',
                'Bochum',
                'Bonn',
                'Bottrop',
                'Dortmund',
                'Duisburg',
                'Düsseldorf',
                'Essen',
                'Gelsenkirchen',
                'Hagen',
                'Hamm',
                'Herne',
                'Cologne',
                'Krefeld',
                'Leverkusen',
                'Mönchengladbach',
                'Mülheim',
                'Münster',
                'Oberhausen',
                'Remscheid',
                'Solingen',
                'Wuppertal'
            ],
            'Saxony' => [
                'Bautzen',
                'Erzgebirgskreis',
                'Görlitz',
                'Leipzig',
                'Meissen',
                'Mittelsachsen',
                'Nordsachsen',
                'Sächsische Schweiz-Osterzgebirge',
                'Vogtlandkreis',
                'Zwickau'
            ],
            'Hesse' => [
                'Bergstraße',
                'Darmstadt-Dieburg',
                'Groß-Gerau',
                'Hochtaunuskreis',
                'Main-Kinzig-Kreis',
                'Main-Taunus-Kreis',
                'Odenwaldkreis',
                'Offenbach',
                'Rheingau-Taunus-Kreis',
                'Wetteraukreis',
                'Gießen',
                'Lahn-Dill-Kreis',
                'Limburg-Weilburg',
                'Marburg-Biedenkopf',
                'Vogelsbergkreis',
                'Fulda',
                'Hersfeld-Rotenburg',
                'Kassel',
                'Schwalm-Eder-Kreis',
                'Werra-Meißner-Kreis',
                'Waldeck-Frankenberg'
            ]
        ];

        foreach ($counties as $stateTitle => $stateCounties) {
            foreach ($stateCounties as $countyTitle) {
                $taxRate = rand(15, 30);
                $this->addSql("
                            INSERT INTO `country_state_county` SET 
                                `title` = ?,
                                `countryStateId` = (SELECT `id` FROM `country_state` WHERE `title` = ? AND `countryCode` = 'DE'),
                                `taxRate` = ?
                            ", [$countyTitle, $stateTitle, $taxRate]);
            }
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql("
            DELETE FROM `country_state_county`;
        ");
    }
}
