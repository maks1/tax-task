function normalize(collection, field = 'id') {
    let newCollection = {};
    _.forEach(collection, (item) => {
        if (!_.isUndefined(item[field])) {
            newCollection[item[field]] = item;
        }
    });

    return newCollection;
}

function deepNormalize(collection, field = 'id') {
    let newCollection = {};
    _.forEach(collection, (item) => {
            newCollection[item[field]] = item;
            for (let key in item) {
                if (_.isArray(item[key])) {
                    newCollection[item[field]][key] = normalize(item[key]);
                }
            }
        }
    );

    return newCollection;
}

function deepNormalizeWithKey(collection, field = 'id', secondKey = field) {
    let newCollection = {};
    _.forEach(collection, (item) => {
            if (!newCollection[item[field]]) {
                newCollection[item[field]] = {};
            }
            newCollection[item[field]][item[secondKey]] = item;
        }
    );

    return newCollection;
}

function windowSize() {
    let myWidth = 0, myHeight = 0;
    if (typeof (window.innerWidth) == 'number') {
        //Non-IE
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
    }
    return {height: myHeight, width: myWidth};
}


const _markerIcons = [];

function getMarkerIcon(boardFormatList, board) {
    if (!_.isUndefined(_markerIcons[board.constructTypeId])) {
        return _markerIcons[board.constructTypeId]
    }

    const icon = {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 10, // size of the board marker
        fillOpacity: 0.9,
        fillColor: !_.isUndefined(boardFormatList[board.constructTypeId]) ? `#${boardFormatList[board.constructTypeId].color}` : 'black',
        strokeColor: '#FFFFFF',
        strokeWeight: 2
    };

    _markerIcons[board.constructTypeId] = icon;

    return icon;
}

const _currentBoardMarkerIcons = [];
const PREFIX = 'r'; // '' - default
const PREFIX_SELECT = 'b'; // '' - default

function getCurrentBoardMarkerIcon(board) {
    if (board.viewPoint) {
        if (!_.isUndefined(_currentBoardMarkerIcons[board.viewPoint])) {
            return _currentBoardMarkerIcons[board.viewPoint]
        }

        const icon = {
            url: `/images/maps/vp/${PREFIX}${_.toLower(board.viewPoint)}.png`,
            scaledSize: new google.maps.Size(20, 20),
            anchor: new google.maps.Point(8, 8),
            // labelOrigin: new google.maps.Point(-1 + index * 10, -8)
        };

        _currentBoardMarkerIcons[board.viewPoint] = icon;

        return icon;
    } else {
        return {
            url: `/images/maps/vp/${PREFIX}ND.png`,
            scaledSize: new google.maps.Size(20, 20),
            anchor: new google.maps.Point(8, 8),
            // labelOrigin: new google.maps.Point(-1 + index * 10, -8)
        };
    }
}

export {
    normalize,
    deepNormalize,
    deepNormalizeWithKey,
    windowSize,
    getMarkerIcon,
    getCurrentBoardMarkerIcon
}
