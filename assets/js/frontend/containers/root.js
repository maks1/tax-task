import React from "react";
import {Route, Switch} from "react-router-dom";
import Routing from '../../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

import ReportPage from './reportPage';

const routes = require('../routes/fos_js_routes');
Routing.setRoutingData(routes);

const Root = () => (
    <div id='content' className='content'>
        <div className="container">
            <Switch>
                <Route path="/" exact component={ReportPage}/>
            </Switch>
        </div>
    </div>
);

export default Root;
