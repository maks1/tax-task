import React from "react";
import PropTypes from "prop-types"
import Table from "react-bootstrap/lib/Table";

const propTypes = {
    states: PropTypes.object.isRequired,
    country: PropTypes.object.isRequired
};

const ReportTable = ({country, states, statistic}) => {
    const countryStatistic = statistic[country.code];
    const tableBody = _.map(countryStatistic.stateStatistics, (currentStateStatistic) => {
        const {stateId, statistic} = currentStateStatistic;
        const {totalAmount, avgAmount, countOfCounties, avgTaxRate} = statistic;
        return (
            <tr key={`state-row-${stateId}`}>
                <td>{states[stateId].title}</td>
                <td className={'text-right'}>{totalAmount}</td>
                <td className={'text-right'}>{avgAmount}</td>
                <td className={'text-right'}>{countOfCounties}</td>
                <td className={'text-right'}>{avgTaxRate}</td>
            </tr>
        )
    });
    const {statistic: totalStatistic} = countryStatistic;
    const {totalAmount, avgAmount, countOfCounties, avgTaxRate} = totalStatistic;
    return (
        <Table responsive>
            <thead>
            <tr className={"active"}>
                <th>State</th>
                <th className={'text-right'}>Amount</th>
                <th className={'text-right'}>Average amount</th>
                <th className={'text-right'}>Count of counties</th>
                <th className={'text-right'}>Average tax rate</th>
            </tr>
            </thead>
            <tbody>
            {tableBody}
            <tr className={'info'}>
                <td><b>{country.title}</b></td>
                <td className={'text-right'}><b>{totalAmount}</b></td>
                <td className={'text-right'}><b>{avgAmount}</b></td>
                <td className={'text-right'}><b>{countOfCounties}</b></td>
                <td className={'text-right'}><b>{avgTaxRate}</b></td>
            </tr>
            </tbody>
        </Table>
    )
};

ReportTable.propTypes = propTypes;
export default ReportTable;