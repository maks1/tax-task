import React, {Fragment} from "react";
import {connect} from 'react-redux';
import PropTypes from "prop-types";
import Row from "react-bootstrap/lib/Row";
import {withRouter} from "react-router-dom";
import MyCombobox from "../components/input/combobox";
import ControlLabel from "react-bootstrap/lib/ControlLabel";
import Col from "react-bootstrap/lib/Col";
import DatePicker, {registerLocale} from 'react-datepicker';

import appActions from "../actions/appActions";
import Loader from "../components/loader/loader";
import ReportTable from "./reportTable";
import {addMonths} from "date-fns";

class ReportPage extends React.PureComponent {

    static propTypes = {
        baseState: PropTypes.object.isRequired,
        countryRepository: PropTypes.object.isRequired,
        countryStateRepository: PropTypes.object.isRequired,
        countryStatisticRepository: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            filter: {
                country: null,
                period: addMonths(new Date(), -1)
            }
        }
    }

    componentDidMount() {
        const {countries} = this.props.countryRepository;
        if (_.isNull(countries)) {
            const {dispatch} = this.props;
            dispatch(appActions.fetchCountryList());
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const {countries: prevCountries} = prevProps.countryRepository;
        const {countries} = this.props.countryRepository;
        if (prevCountries !== countries) {
            Promise.resolve(
                this.setState(state => ({...state, filter: {...state.filter, country: countries[0]}}))
            )
                .then(() => {
                        this.fetchStatesList();
                        this.fetchCountryStatistic();
                    }
                )
        }
    }

    handlePeriodChange = (newPeriodStart) => {
        Promise.resolve(
            this.setState(state => ({...state, filter: {...state.filter, period: newPeriodStart}}))
        )
            .then(() => {
                this.fetchCountryStatistic()
            })
    }

    fetchStatesList = () => {
        const {dispatch} = this.props;
        const {filter} = this.state;
        dispatch(appActions.fetchStatesList(filter.country));
    }

    fetchCountryStatistic = () => {
        const {dispatch} = this.props;
        const {filter} = this.state;
        dispatch(appActions.fetchCountryStatistic(filter));
    }

    render() {
        const {fetching} = this.props.countryRepository;
        const {fetching: stateFetching} = this.props.countryStateRepository;
        const {fetching: statisticFetching} = this.props.countryStatisticRepository;
        const {filter} = this.state;
        let data;
        switch (true) {
            case fetching:
                data = (
                    <Loader/>
                );
                break;
            default:
                const {countries} = this.props.countryRepository;
                const {states} = this.props.countryStateRepository;
                const {statistic} = this.props.countryStatisticRepository;
                data = (
                    <Fragment>
                        <Row className={'vertical-align'}>
                            <Col sm={1}>
                                <img src='/images/logo.png'/>
                            </Col>
                            <Col sm={1}>
                                <ControlLabel>Country</ControlLabel>
                            </Col>
                            <Col sm={4}>
                                <MyCombobox
                                    onChange={() => {
                                    }}
                                    data={!_.isNull(countries) ? countries : []}
                                    textField={'title'}
                                    value={filter.country}
                                    placeHolder={'Select country'}
                                />
                            </Col>
                            <Col sm={1}>
                                <ControlLabel>Month</ControlLabel>
                            </Col>
                            <Col sm={2}>
                                <DatePicker
                                    className="form-control"
                                    showMonthYearPicker
                                    onChange={this.handlePeriodChange}
                                    dateFormat="MM-yyyy"
                                    selected={filter.period}
                                />
                            </Col>
                        </Row>
                        <Row style={{paddingTop: '15px'}}>
                            {!stateFetching && !statisticFetching ?
                                <Col sm={12}>
                                    <ReportTable
                                        states={states}
                                        statistic={statistic}
                                        country={filter.country}
                                    />
                                </Col>
                                : <Loader/>}
                        </Row>
                    </Fragment>
                );
        }
        return (
            <Fragment>
                {data}
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        baseState: state.baseState,
        countryRepository: state.countryRepository,
        countryStateRepository: state.countryStateRepository,
        countryStatisticRepository: state.countryStatisticRepository
    }
};

export default withRouter(connect(mapStateToProps)(ReportPage));