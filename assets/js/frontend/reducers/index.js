import {combineReducers} from "redux";

import baseReducer from "./baseReducer";
import countryRepositoryReducer from "./countryRepositoryReducer";
import countryStateRepositoryReducer from "./countryStateRepositoryReducer";
import countryStatisticRepositoryReducer from "./countryStatisticRepositoryReducer";

// Combine all reducers you may have here
export default combineReducers({
    baseState: baseReducer,
    countryRepository: countryRepositoryReducer,
    countryStateRepository: countryStateRepositoryReducer,
    countryStatisticRepository: countryStatisticRepositoryReducer
});
