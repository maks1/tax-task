import {COUNTRY_INFO_FETCHING, COUNTRY_INFO_RESERVED} from "../constants/appActionConstants";

export const initialState = {
    countries: null,
    fetching: true,
    success: true,
    error: null
};

export default function countryRepositoryReducer(state = initialState, action) {
    switch (action.type) {
        case COUNTRY_INFO_FETCHING:
            return {...state, fetching: true};
        case COUNTRY_INFO_RESERVED:
            const {list} = action.data;
            return {...state, countries: list, fetching: false};
        default:
            return state;
    }
}
