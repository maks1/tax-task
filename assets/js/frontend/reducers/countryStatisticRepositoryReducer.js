import {
    COUNTRY_STATISTIC_FETCHING,
    COUNTRY_STATISTIC_RESERVED
} from "../constants/appActionConstants";

export const initialState = {
    statistic: null,
    fetching: true,
    success: true,
    error: null
};

const getStatistic = (countryStatistic) => {
    const statistic = {};
    statistic[countryStatistic.countryCode] = countryStatistic;

    return statistic;
};

export default function countryStatisticRepositoryReducer(state = initialState, action) {
    switch (action.type) {
        case COUNTRY_STATISTIC_FETCHING:
            return {...state, fetching: true};
        case COUNTRY_STATISTIC_RESERVED:
            const {countryStatistic} = action.data;
            const statistic = getStatistic(countryStatistic);
            return {...state, statistic, fetching: false};
        default:
            return state;
    }
}
