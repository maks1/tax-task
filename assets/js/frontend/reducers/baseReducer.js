export const initialState = {
    baseUrl: '/',
    location: '/',
    locale: null
};

export default function baseReducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
    }
}
