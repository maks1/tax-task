import {STATE_LIST_FETCHING, STATE_LIST_RESERVED} from "../constants/appActionConstants";
import {normalize} from "../helpers/commonFunctions";

export const initialState = {
    states: null,
    fetching: true,
    success: true,
    error: null
};

export default function countryStateRepositoryReducer(state = initialState, action) {
    switch (action.type) {
        case STATE_LIST_FETCHING:
            return {...state, fetching: true};
        case STATE_LIST_RESERVED:
            const {list} = action.data;
            return {...state, states: normalize(list), fetching: false};
        default:
            return state;
    }
}
