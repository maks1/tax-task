import routing from '../../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import {
    COUNTRY_INFO_FETCHING,
    COUNTRY_INFO_RESERVED,
    COUNTRY_STATISTIC_FETCHING,
    COUNTRY_STATISTIC_RESERVED,
    STATE_LIST_FETCHING,
    STATE_LIST_RESERVED
} from "../constants/appActionConstants";

import {format} from "date-fns";

const API_DATE_FORMAT = 'yyyy-MM-dd';

const appActions = {
    fetchCountryList: () => dispatch => {
        dispatch({type: COUNTRY_INFO_FETCHING});
        return fetch(routing.generate('api_country_list'), {
            method: "GET",
        })
            .then(onSuccess)
            .then(data => {
                dispatch({
                    type: COUNTRY_INFO_RESERVED,
                    data
                });
            });
    },
    fetchStatesList: (country) => dispatch => {
        dispatch({type: STATE_LIST_FETCHING});
        const {code: countryCode} = country;
        return fetch(routing.generate('api_country_state_list', {countryCode}), {
            method: "GET",
        })
            .then(onSuccess)
            .then(data => {
                dispatch({
                    type: STATE_LIST_RESERVED,
                    data
                });
            });
    },
    fetchCountryStatistic: (filter) => dispatch => {
        dispatch({type: COUNTRY_STATISTIC_FETCHING});
        const {country, period} = filter;
        return fetch(routing.generate('report_statistic', {
            countryCode: country.code,
            period: format(period, API_DATE_FORMAT)
        }), {
            method: "GET",
        })
            .then(onSuccess)
            .then(data => {
                dispatch({
                    type: COUNTRY_STATISTIC_RESERVED,
                    data
                });
            });
    }
};

const onSuccess = (response) => {
    switch (response.status) {
        case 200:
            return response.json();
        case 401:
            return {
                success: false,
                error: 'Unauthorized'
            };
        default:
            try {
                return response.json();
            } catch (e) {
                return {
                    success: false,
                    error: 'Unknown error'
                }
            }

    }
};

export default appActions;
