import React from 'react'
import PropTypes from 'prop-types'
import {pure} from 'recompose';

const propTypes = {
    style: PropTypes.object
};

const Loader = ({style}) => (
    <div style={{...{textAlign: 'center', lineHeight: 3}, ...style}}>
        <i className='fas fa-spinner fa-spin'/>
    </div>
);

Loader.propTypes = propTypes;

export default pure(Loader);