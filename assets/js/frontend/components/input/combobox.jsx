import React from 'react';
import PropTypes from 'prop-types';

import Combobox from 'react-widgets/lib/Combobox';

export default class MyCombobox extends React.PureComponent {

    static propTypes = {
        placeHolder: PropTypes.string,
        valueField: PropTypes.string,
        textField: PropTypes.string,
        data: PropTypes.array,
        value: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.object]),
        onChange: PropTypes.func.isRequired,
        onSelect: PropTypes.func,
        emptyListMessages: PropTypes.string
    };

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };
    }

    toggleCombobox = (isOpen) => {
        this.setState(state => ({...state, isOpen: isOpen}))
    };

    render() {
        const {value, valueField, textField, placeHolder, data, onChange, onSelect, emptyListMessages} = this.props;
        const {isOpen} = this.state;
        let dataToShow;
        if (!value && !isOpen) {
            const dataPlaceHolder = {};
            dataPlaceHolder[valueField] = null;
            dataPlaceHolder[textField] = placeHolder;
            const tempArray = [];
            tempArray.push(dataPlaceHolder);
            dataToShow = _.concat(tempArray, data);
        } else {
            dataToShow = data
        }
        return (
            <Combobox
                valueField={valueField}
                textField={textField}
                data={dataToShow}
                onToggle={this.toggleCombobox}
                value={value}
                onChange={onChange ? onChange : undefined}
                onSelect={onSelect ? onSelect : undefined}
                messages={{
                    emptyList: emptyListMessages ? emptyListMessages : ''
                }}
            />
        );
    }
}
