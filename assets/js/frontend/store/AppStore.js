import {createStore, applyMiddleware, compose} from "redux";
import thunkMiddleware from 'redux-thunk'
import {normalize, deepNormalizeWithKey} from "../helpers/commonFunctions";

import reducers from "../reducers";

const middleware = [
    thunkMiddleware
];

export default function configureStore() {
    // use devtools if we are in a browser and the extension is enabled
    let composeEnhancers =
        (typeof window !== "undefined" &&
            window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
        compose;

    if (process.env.NODE_ENV === `development`) {
        const {logger} = require(`redux-logger`);
        middleware.push(logger);
    }

    return createStore(
        reducers,
        composeEnhancers(applyMiddleware(...middleware))
    );
}
