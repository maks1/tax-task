import React from "react";
import ReactDOM from 'react-dom'
import {Provider} from "react-redux";
import Root from "../containers/root";
import {BrowserRouter} from "react-router-dom";
import AppStore from "../store/AppStore"
import "core-js/modules/es.promise";

// We render a different router depending on whether we are rendering server side
// or client side.
let Router = props => (
    <BrowserRouter>{props.children}</BrowserRouter>
);
const rootElement = document.getElementById('react-container')
ReactDOM.render(
    <Provider store={AppStore()}>
        <Router>
            <Root/>
        </Router>
    </Provider>, rootElement
);
